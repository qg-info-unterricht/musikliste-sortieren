
/**
 * Beschreiben Sie hier die Klasse Song.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Song
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private String artist;
    private String title;
    private String year;
    private int rating; 
    
    /**
     * Konstruktor für Objekte der Klasse Song
     */
    public Song(String artist, String title, String year)
    {
       this.artist=artist;
       this.title=title;
       this.year=year;
       this.rating=(int) (Math.random() * 100) + 1;
    }
    
    @Override
    public String toString() {
        return(artist + " - " + title + " (" + year + "): " + rating);
    }

    
}
