import java.io.FileReader;
import java.util.Arrays;
import com.opencsv.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Beschreiben Sie hier die Klasse Musikliste.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Musikliste
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private ArrayList<Song> songs = new ArrayList<>();
    private int inputFileLines = 0;

    /**
     * Konstruktor für Objekte der Klasse Musikliste
     */
    public Musikliste() throws Exception
    {
        this.readInput("tracks.txt", ';');
        printList(songs);
    }
    
    public void printList(ArrayList<Song> liste) {
        for(Song s: liste) {
            System.out.println(s);
        }
    }

    /**
     * Beispielhafte Methode zum Einlesen von CSV Daten
     * in die ArrayList von String Arrays ("input").
     *  
     * Dokumentation: http://opencsv.sourceforge.net/
     **/
    public void readInput(String filename, char fieldSeperator) throws Exception {
        CSVParser myParser = new CSVParserBuilder()
            .withSeparator(fieldSeperator)  // Trennzeichen
            .build();

        CSVReader reader = new CSVReaderBuilder(new FileReader(filename))
            .withCSVParser(myParser)
            .build();

        String[] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            // nextLine[] is an array of values from the line
            this.inputFileLines++;
            if (nextLine.length == 4) {
                songs.add(new Song(nextLine[2], nextLine[3], nextLine[0]));
            }
        }
    }    
   
}
